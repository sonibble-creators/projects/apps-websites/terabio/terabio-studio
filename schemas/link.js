export default {
  name: 'link',
  title: 'Links',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Link Name',
      type: 'string',
    },
    {
      name: 'url',
      title: 'URL',
      type: 'string',
    },
    {
      name: 'showAs',
      title: 'Show As',
      type: 'string',
      options: {
        list: [
          { title: 'Icon', value: 'icon' },
          { title: 'Block', value: 'block' },
        ],
      },
    },
    {
      name: 'isShow',
      title: 'Is Show the link',
      type: 'boolean',
    },
    {
      name: 'totalViews',
      title: 'Total View of link',
      type: 'number',
      initialValue: 0,
    },
    {
      name: 'bio',
      title: 'Bio Owner',
      type: 'reference',
      to: { type: 'bio' },
    },
  ],
}
