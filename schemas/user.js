export default {
  name: 'user',
  title: 'Users',
  type: 'document',
  fields: [
    {
      name: 'email',
      title: 'Email address',
      type: 'string',
    },
    {
      name: 'password',
      title: 'Password',
      type: 'string',
    },
    {
      name: 'providers',
      title: 'Providers',
      type: 'array',
      of: [{ type: 'string' }],
      options: {
        list: [
          { title: 'Email', value: 'email' },
          { title: 'Google', value: 'google' },
        ],
      },
    },
    {
      name: 'status',
      title: 'Status',
      type: 'string',
      options: {
        list: [
          { title: 'Active', value: 'active' },
          { title: 'Disable', value: 'disable' },
        ],
      },
    },
    {
      name: 'role',
      title: 'Roles of user',
      type: 'string',
      options: {
        list: [
          { title: 'Admin', value: 'admin' },
          { title: 'User', value: 'user' },
        ],
      },
    },
  ],
}
