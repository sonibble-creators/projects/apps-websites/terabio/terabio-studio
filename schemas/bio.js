export default {
  name: 'bio',
  title: 'Bios',
  type: 'document',
  fields: [
    {
      name: 'photo',
      title: 'Photo',
      type: 'image',
    },
    {
      name: 'name',
      title: 'Name',
      type: 'string',
    },
    {
      name: 'link',
      title: 'Link Url',
      type: 'string',
    },
    {
      name: 'about',
      title: 'About',
      type: 'text',
    },
    {
      name: 'button',
      title: 'Button Settings & Styles',
      type: 'object',
      fields: [
        {
          name: 'icon',
          title: 'Icon Button',
          type: 'string',
          options: {
            list: [
              { title: 'Solid', value: 'solid' },
              { title: 'Color Full', value: 'colorFull' },
              { title: 'Rounded', value: 'rounded' },
              { title: 'Outline', value: 'outline' },
            ],
          },
        },
        {
          name: 'block',
          title: 'Block Button',
          type: 'string',
          options: {
            list: [
              { title: 'Solid', value: 'solid' },
              { title: 'Color Full', value: 'colorFull' },
              { title: 'Rounded', value: 'rounded' },
              { title: 'Outline', value: 'outline' },
            ],
          },
        },
      ],
    },
    {
      name: 'watermark',
      title: 'Watermark',
      type: 'boolean',
    },
    {
      name: 'theme',
      title: 'Themes & Settings',
      type: 'object',
      fields: [
        {
          name: 'style',
          title: 'Styles',
          type: 'string',
          options: {
            list: [
              { title: 'Flat', value: 'flat' },
              { title: 'Gradient', value: 'gradient' },
              { title: 'Image', value: 'image' },
            ],
          },
        },
        {
          name: 'color',
          title: 'Color',
          type: 'string',
        },
        {
          name: 'secondaryColor',
          title: 'Secondary Color',
          type: 'string',
        },
        {
          name: 'image',
          title: 'Image',
          type: 'image',
        },
      ],
    },
    {
      name: 'user',
      title: 'Owner',
      type: 'reference',
      to: { type: 'user' },
    },
  ],
}
